﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Helpers
{
    public class Utils
    {
        public static TunnelAppDataContext getDBConnection()
        {
            return new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        }
    }
}
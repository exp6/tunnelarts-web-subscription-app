﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunnelApp.Subscription.Configuration
{
    interface IConfigurable
    {
        void Configure();
    }
}
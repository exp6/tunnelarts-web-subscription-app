﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace TunnelApp.Subscription.Models.Faces
{
    public class FacesFormModel
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Tunnel")]
        public Guid IdTunnel { get; set; }

        [DisplayName("Reference Chainage")]
        public string RefChainage { get; set; }

        [DisplayName("Orientation (º)")]
        public decimal Orientation { get; set; }

        [DisplayName("Inclination (%)")]
        public decimal Inclination { get; set; }

        public Guid IdFace { get; set; }

        public string userName { get; set; }
    }
}
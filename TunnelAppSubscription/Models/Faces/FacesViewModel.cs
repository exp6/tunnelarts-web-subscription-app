﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.Faces
{
    public class FacesViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public FacesFormModel FORM { get; set; }
        public IEnumerable<Face> faces { get; set; }
        public IEnumerable<SelectListItem> tunnels { get; set; }
        public Face face { get; set; }

        public FacesViewModel()
        {
            faces = db.Faces.OrderBy(x => x.Name);
        }

        public FacesViewModel(string userName)
        {
            tunnels = db.Tunnels.OrderBy(x => x.Project.Name).Select(x => new SelectListItem()
            {
                Text =  x.Project.Name + " - " + x.Name,
                Value = x.id.ToString()
            });
        }

        public FacesViewModel(Guid idFace)
        {
            face = db.Faces.Single(x => x.id == idFace);
            FORM = new FacesFormModel();
            FORM.IdFace = face.id;
            FORM.Name = face.Name;
            FORM.IdTunnel = face.IdTunnel;
            FORM.RefChainage = face.RefChainage;
            FORM.Orientation = face.Orientation;
            FORM.Inclination = face.Inclination;

            tunnels = db.Tunnels.OrderBy(x => x.Project.Name).Select(x => new SelectListItem()
            {
                Text = x.Project.Name + " - " + x.Name,
                Value = x.id.ToString()
            });
        }

        public FacesViewModel(FacesFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Tunnel t = db.Tunnels.Single(x => x.id == FORM.IdTunnel);

            Face nuevo = new Face
            {
                id = Guid.NewGuid(),
                Tunnel = t,
                Name = FORM.Name,
                RefChainage = FORM.RefChainage,
                Orientation = FORM.Orientation,
                Inclination = FORM.Inclination,
                createdAt = DateTime.Now,
                CreatedBy = FORM.userName
            };
            db.Faces.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Tunnel t = db.Tunnels.Single(x => x.id == FORM.IdTunnel);

            Face f = db.Faces.Single(x => x.id == FORM.IdFace);
            f.Tunnel = t;
            f.Name = FORM.Name;
            f.RefChainage = FORM.RefChainage;
            f.Orientation = FORM.Orientation;
            f.Inclination = FORM.Inclination;
            db.SubmitChanges();
        }

        public void delete()
        {
            Face f = db.Faces.Single(x => x.id == FORM.IdFace);
            db.Faces.DeleteOnSubmit(f);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.ModelTraining
{
    public class ModelTrainingViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public IEnumerable<Tunnel> tunnels { get; set; }
        public Tunnel tunnel { get; set; }
        private double rmse { get; set; }

        public ModelTrainingViewModel()
        {
            tunnels = db.Tunnels.OrderBy(x => x.Name);
        }

        public ModelTrainingViewModel(Guid idTunnel)
        {
            tunnel = db.Tunnels.Single(x => x.id == idTunnel);
        }

        public ModelTrainingViewModel(Guid idTunnel, double _rmse)
        {
            tunnel = db.Tunnels.Single(x => x.id == idTunnel);
            rmse = _rmse;
        }

        public int saveData()
        {
            int res = 0;

            if(tunnel.RockTypePredictionTrainings.Count() == 0)
            {
                RockTypePredictionTraining nuevo = new RockTypePredictionTraining
                {
                    id = Guid.NewGuid(),
                    Tunnel = tunnel,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    Rmse = rmse,
                    Precision = 0
                };
                db.RockTypePredictionTrainings.InsertOnSubmit(nuevo);
                
            }
            else
            {
                RockTypePredictionTraining rtpdt = tunnel.RockTypePredictionTrainings.FirstOrDefault();
                rtpdt.UpdatedAt = DateTime.Now;
                rtpdt.Rmse = rmse;
                rtpdt.Precision = 0;
                res = 1;
            }

            db.SubmitChanges();
            
            return res;
        }

        public RockTypePredictionTraining getPrediction(Guid idTunnel)
        {
            return db.RockTypePredictionTrainings.SingleOrDefault(x => x.IdTunnel == idTunnel);
        }
    }
}
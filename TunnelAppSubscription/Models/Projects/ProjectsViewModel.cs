﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;
using System.Web.Mvc;
using System.Globalization;

namespace TunnelApp.Subscription.Models.Projects
{
    public class ProjectsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public ProjectsFormModel FORM { get; set; }
        public IEnumerable<Project> projects { get; set; }
        public Project project { get; set; }
        public IEnumerable<SelectListItem> clients { get; set; }

        public ProjectsViewModel() 
        {
            projects = db.Projects.OrderBy(x => x.Name);
        }

        public ProjectsViewModel(string userName)
        {
            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });
        }

        public ProjectsViewModel(Guid idProject)
        {
            
            project = db.Projects.Single(x => x.id == idProject);
            FORM = new ProjectsFormModel();
            FORM.IdProject = project.id;
            FORM.Code = project.Code;
            FORM.Name = project.Name;
            FORM.Description = project.Description;
            FORM.startDate = project.StartDate.ToShortDateString();
            FORM.endDate = project.FinishDate.ToShortDateString();

            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });
        }

        public ProjectsViewModel(ProjectsFormModel form)
        {
            
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Client ct = db.Clients.Single(x => x.id == FORM.IdClient);

            Project nuevo = new Project
            {
                id = Guid.NewGuid(),
                Client = ct,
                Code = FORM.Code,
                Name = FORM.Name,
                Description = FORM.Description,
                StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                FinishDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                createdAt = DateTime.Now,
                CreatedBy = FORM.userName,
                deleted = false
            };
            db.Projects.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Project p = db.Projects.Single(x => x.id == FORM.IdProject);
            p.Code = FORM.Code;
            p.Name = FORM.Name;
            p.Description = FORM.Description;
            p.StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            p.FinishDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            db.SubmitChanges();
        }

        public void delete()
        {
            Project p = db.Projects.Single(x => x.id == FORM.IdProject);
            db.Projects.DeleteOnSubmit(p);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
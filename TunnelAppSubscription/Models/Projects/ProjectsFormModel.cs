﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace TunnelApp.Subscription.Models.Projects
{
    public class ProjectsFormModel
    {
        [DisplayName("Code")]
        public string Code { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Start Date")]
        public string startDate { get; set; }

        [DisplayName("Finish Date")]
        public string endDate { get; set; }

        [DisplayName("Client")]
        public Guid IdClient { get; set; }

        public Guid IdProject { get; set; }

        public string userName { get; set; }
    }
}
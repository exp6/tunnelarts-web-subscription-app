﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace TunnelApp.Subscription.Models.Tunnels
{
    public class TunnelsFormModel
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Project")]
        public Guid IdProject { get; set; }

        [DisplayName("ESR")]
        public Guid IdEsr { get; set; }

        [DisplayName("Span")]
        public double? Span { get; set; }

        public Guid IdTunnel { get; set; }

        public string userName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.Tunnels
{
    public class TunnelsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public TunnelsFormModel FORM { get; set; }
        public IEnumerable<Tunnel> tunnels { get; set; }
        public IEnumerable<SelectListItem> projects { get; set; }
        public IEnumerable<SelectListItem> esrs { get; set; }
        public Tunnel tunnel { get; set; }

        public TunnelsViewModel() 
        {
            tunnels = db.Tunnels.OrderBy(x => x.Name);
        }

        public TunnelsViewModel(string userName)
        {
            projects = db.Projects.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });

            esrs = db.Esrs.Select(x => new SelectListItem()
            {
                Text = x.Description,
                Value = x.IdEsr.ToString()
            });
        }

        public TunnelsViewModel(Guid idTunnel)
        {
            tunnel = db.Tunnels.Single(x => x.id == idTunnel);
            FORM = new TunnelsFormModel();
            FORM.IdTunnel = tunnel.id;
            FORM.Name = tunnel.Name;
            FORM.Span = tunnel.Span;
            FORM.IdEsr = tunnel.IdEsr;
            FORM.IdProject = tunnel.IdProject;

            projects = db.Projects.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });

            esrs = db.Esrs.Select(x => new SelectListItem()
            {
                Text = x.Description,
                Value = x.IdEsr.ToString()
            });
        }

        public TunnelsViewModel(TunnelsFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Project p = db.Projects.Single(x => x.id == FORM.IdProject);
            Esr e = db.Esrs.Single(x => x.IdEsr == FORM.IdEsr);

            Tunnel nuevo = new Tunnel
            {
                id = Guid.NewGuid(),
                Project = p,
                Esr = e,
                Name = FORM.Name,
                Span = FORM.Span,
                createdAt = DateTime.Now,
                CreatedBy = FORM.userName,
                deleted = false
            };
            db.Tunnels.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Project p = db.Projects.Single(x => x.id == FORM.IdProject);
            Esr e = db.Esrs.Single(x => x.IdEsr == FORM.IdEsr);

            Tunnel t = db.Tunnels.Single(x => x.id == FORM.IdTunnel);
            t.Project = p;
            t.Esr = e;
            t.Name = FORM.Name;
            t.Span = FORM.Span;

            db.SubmitChanges();
        }

        public void delete()
        {
            Tunnel t = db.Tunnels.Single(x => x.id == FORM.IdTunnel);
            db.Tunnels.DeleteOnSubmit(t);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
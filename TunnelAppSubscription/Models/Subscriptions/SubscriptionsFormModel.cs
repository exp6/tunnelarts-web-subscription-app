﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace TunnelApp.Subscription.Models.Subscriptions
{
    public class SubscriptionsFormModel
    {
        [DisplayName("Client")]
        public Guid IdClient { get; set; }

        [DisplayName("Subscription Module")]
        public Guid IdSubscription { get; set; }

        [DisplayName("Client App Code")]
        public string clientAppCode { get; set; }

        [DisplayName("Start Date")]
        public string startDate { get; set; }

        [DisplayName("Expire Date")]
        public string endDate { get; set; }

        [DisplayName("Report Type")]
        public int ReportType { get; set; }

        public string SubModulesIds { get; set; }

        public Guid IdClientSubscription { get; set; }

        public string userName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Repository;
using System.Globalization;
using System.Configuration;

namespace TunnelApp.Subscription.Models.Subscriptions
{
    public class SubscriptionsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public SubscriptionsFormModel FORM { get; set; }
        public IEnumerable<ClientSubscription> subscriptions { get; set; }
        public IEnumerable<SelectListItem> clients { get; set; }
        public IEnumerable<SelectListItem> modules { get; set; }
        public ClientSubscription subscription { get; set; }

        public List<SubscriptionModule> totalModules { get; set; }
        public List<SubscriptionModule> availableModules { get; set; }
        public List<SubscriptionModule> addedModules { get; set; }

        public string appUrlPath { get; set; }

        public IEnumerable<SelectListItem> reportTypes { get; set; }

        public SubscriptionsViewModel() 
        {
            subscriptions = db.ClientSubscriptions.OrderByDescending(x => x.CreatedAt);
            appUrlPath = ConfigurationManager.AppSettings["TunnelAppUrlPath"].ToString();
        }

        public SubscriptionsViewModel(string userName)
        {
            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });

            modules = db.Subscriptions.OrderBy(x => x.TierName).Select(x => new SelectListItem()
            {
                Text = x.TierName,
                Value = x.IdSubscription.ToString()
            });

            appUrlPath = ConfigurationManager.AppSettings["TunnelAppUrlPath"].ToString();

            reportTypes = db.SubscriptionReportTypes.OrderBy(x => x.Code).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Code.ToString()
            });
        }

        public SubscriptionsViewModel(Guid IdClientSubscription)
        {
            subscription = db.ClientSubscriptions.Single(x => x.IdClientSubscription == IdClientSubscription);

            totalModules = db.SubscriptionModules.OrderBy(x => x.Module.Name).Where(x => x.IdSubscription == subscription.IdSubscription).ToList();
            addedModules = db.ClientSubscriptionModules.Where(x => x.IdClientSubscription == subscription.IdClientSubscription).Select(x => x.SubscriptionModule).ToList();
            availableModules = totalModules.Except(addedModules).Union(addedModules.Except(totalModules)).ToList();
            
            FORM = new SubscriptionsFormModel();
            FORM.IdClientSubscription = subscription.IdClientSubscription;
            FORM.IdClient = subscription.IdClient;
            FORM.IdSubscription = subscription.IdSubscription;
            FORM.startDate = subscription.StartDate.ToString("dd-MM-yyyy");
            FORM.endDate = subscription.ExpireDate.ToString("dd-MM-yyyy");
            FORM.ReportType = subscription.ReportType ?? 0;

            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.id.ToString()
            });

            modules = db.Subscriptions.OrderBy(x => x.TierName).Select(x => new SelectListItem()
            {
                Text = x.TierName,
                Value = x.IdSubscription.ToString()
            });

            appUrlPath = ConfigurationManager.AppSettings["TunnelAppUrlPath"].ToString();

            reportTypes = db.SubscriptionReportTypes.OrderBy(x => x.Code).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Code.ToString()
            });
        }

        public SubscriptionsViewModel(SubscriptionsFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Client ct = db.Clients.Single(x => x.id == FORM.IdClient);
            TunnelApp.Repository.Subscription sb = db.Subscriptions.Single(x => x.IdSubscription == FORM.IdSubscription);

            ClientSubscription nuevo = new ClientSubscription
            {
                IdClientSubscription = Guid.NewGuid(),
                Client = ct,
                Subscription = sb,
                StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                ExpireDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                CreatedAt = DateTime.Now,
                ReportType = FORM.ReportType
            };
            db.ClientSubscriptions.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            ClientSubscription c = db.ClientSubscriptions.Single(x => x.IdClientSubscription == FORM.IdClientSubscription);
            Client ct = db.Clients.Single(x => x.id == FORM.IdClient);
            TunnelApp.Repository.Subscription sb = db.Subscriptions.Single(x => x.IdSubscription == FORM.IdSubscription);
            c.Client = ct;
            c.Subscription = sb;
            c.StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            c.ExpireDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            c.ReportType = FORM.ReportType;
            db.SubmitChanges();
        }

        public void delete()
        {
            ClientSubscription c = db.ClientSubscriptions.Single(x => x.IdClientSubscription == FORM.IdClientSubscription);
            List<ClientSubscriptionModule> cms = db.ClientSubscriptionModules.Where(x => x.IdClientSubscription == c.IdClientSubscription).ToList();

            db.ClientSubscriptionModules.DeleteAllOnSubmit(cms);
            db.ClientSubscriptions.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        public void addSubModules()
        {
            List<Guid> subModulesIds = FORM.SubModulesIds == null ? new List<Guid> { Guid.Empty } : FORM.SubModulesIds.Split(';').Select(s => Guid.Parse(s)).ToList();
            List<Guid> totalElements = db.ClientSubscriptionModules.Where(x => x.IdClientSubscription == FORM.IdClientSubscription).Select(x => x.IdSubscriptionModule).ToList();
            List<Guid> elements2DeleteIds = totalElements.Except(subModulesIds).Union(subModulesIds.Except(totalElements)).ToList();
            List<ClientSubscriptionModule> elements2Delete = db.ClientSubscriptionModules.Where(x => elements2DeleteIds.Contains(x.IdSubscriptionModule) && x.IdClientSubscription == FORM.IdClientSubscription).ToList();

            db.ClientSubscriptionModules.DeleteAllOnSubmit(elements2Delete);

            ClientSubscriptionModule newClientSubscriptionModule;
            ClientSubscription clientSubscription = db.ClientSubscriptions.Single(x => x.IdClientSubscription == FORM.IdClientSubscription);
            SubscriptionModule subscriptionModule;

            IQueryable<ClientSubscriptionModule> categoriaClientes = db.ClientSubscriptionModules.Where(x => x.IdClientSubscription == FORM.IdClientSubscription);

            for (int i = 0; i < subModulesIds.Count(); i++)
            {
                if (categoriaClientes.Any(x => x.IdSubscriptionModule == subModulesIds[i]) || subModulesIds[i] == Guid.Empty)
                {
                    continue;
                }
                else
                {
                    subscriptionModule = db.SubscriptionModules.Single(x => x.IdSubscriptionModule == subModulesIds[i]);

                    newClientSubscriptionModule = new ClientSubscriptionModule
                    {
                        IdClientSubscriptionModule = Guid.NewGuid(),
                        ClientSubscription = clientSubscription,
                        SubscriptionModule = subscriptionModule,
                        CreatedAt = DateTime.Now,
                        CreatedBy = FORM.userName
                    };

                    db.ClientSubscriptionModules.InsertOnSubmit(newClientSubscriptionModule);
                }
            }
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
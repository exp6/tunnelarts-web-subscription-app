﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelAppSubscription.Models.Subscriptions
{
    public class ConsumptionViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public ClientSubscription subscription { get; set; }
        public IEnumerable<Project> projects { get; set; }
        public Project project { get; set; }
        public IEnumerable<Tunnel> tunnels { get; set; }
        public ConsumptionFormModel FORM { get; set; }

        public ConsumptionViewModel(Guid IdClientSubscription)
        {
            subscription = db.ClientSubscriptions.Single(x => x.IdClientSubscription == IdClientSubscription);
            projects = subscription.Client.Projects;
        }

        public ConsumptionViewModel(Guid IdClientSubscription, Guid IdProject)
        {
            subscription = db.ClientSubscriptions.Single(x => x.IdClientSubscription == IdClientSubscription);
            project = db.Projects.Single(x => x.id == IdProject);
            tunnels = project.Tunnels;

            FORM = new ConsumptionFormModel();
            FORM.IdProject = IdProject;
            FORM.Balance = project.Balance ?? 0;
            FORM.IdTunnels = tunnels.Select(x => x.id).ToArray();
            FORM.Balances = tunnels.Select(x => x.Balance ?? 0).ToArray();
            FORM.IdClientSubscription = IdClientSubscription;
        }

        public ConsumptionViewModel(ConsumptionFormModel form)
        {
            FORM = form;
        }

        public void updateChanges()
        {
            Project p = db.Projects.Single(x => x.id == FORM.IdProject);
            p.Balance = FORM.Balance;

            for (int i = 0; i < FORM.IdTunnels.Length; i++)
            {
                Tunnel t = db.Tunnels.Single(x => x.id == FORM.IdTunnels[i]);
                t.Balance = FORM.Balances[i];
            }

            db.SubmitChanges();
        }
    }
}
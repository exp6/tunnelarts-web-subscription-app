﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunnelAppSubscription.Models.Subscriptions
{
    public class ConsumptionFormModel
    {
        public Guid IdProject { get; set; }

        public double Balance { get; set; }

        public Guid[] IdTunnels { get; set; }

        public double[] Balances { get; set; }

        public Guid IdClientSubscription { get; set; }
    }
}
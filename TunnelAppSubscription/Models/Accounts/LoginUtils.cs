﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.Accounts
{
    public class LoginUtils
    {
        public static User getCustomUser(string username)
        {
            TunnelAppDataContext db = new TunnelAppDataContext().WithConnectionStringFromConfiguration();

            User customUser = db.Users.Single(x => x.UserName == username);

            if (customUser == null)
            {
                customUser = new User();
                customUser.UserName = username;
            }

            return customUser;
        }
    }
}
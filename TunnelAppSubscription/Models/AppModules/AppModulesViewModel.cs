﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.AppModules
{
    public class AppModulesViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public AppModulesFormModel FORM { get; set; }
        public IEnumerable<Client> clients { get; set; }
        public Client client { get; set; }

        public AppModulesViewModel() 
        {
            clients = db.Clients.OrderBy(x => x.Name);
        }

        public AppModulesViewModel(string userName)
        {
        }

        public AppModulesViewModel(Guid idClient)
        {
            client = db.Clients.Single(x => x.id == idClient);
            FORM = new AppModulesFormModel();
            //FORM.IdClient = client.IdClient;
            //FORM.Nombre = client.Nombre;
            //FORM.CorreoContacto = client.CorreoContacto;
            //FORM.Telefono = client.Telefono;
        }

        public AppModulesViewModel(AppModulesFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Client nuevo = new Client
            {
                id = Guid.NewGuid(),
                //Nombre = FORM.Nombre,
                //CorreoContacto = FORM.CorreoContacto,
                //Telefono = FORM.Telefono,
                //Estado = 1
            };
            db.Clients.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            //Client d = db.Clients.Single(x => x.IdClient == FORM.idClient);
            //d.Nombre = FORM.Nombre;
            //d.CorreoContacto = FORM.CorreoContacto;
            //d.Telefono = FORM.Telefono;
            db.SubmitChanges();
        }

        public void delete()
        {
            //Client c = db.Clients.Single(x => x.IdClient == FORM.idClient);
            //db.Clients.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace TunnelApp.Subscription.Models.Clients
{
    public class ClientsFormModel
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Website")]
        public string Website { get; set; }

        [DisplayName("Logo Img URL")]
        public string Logo { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Company's Color")]
        public string Color { get; set; }

        [DisplayName("Category")]
        public Guid IdClientType { get; set; }

        [DisplayName("Logo")]
        public Guid IdLogo { get; set; }

        public Guid IdClient { get; set; }
        
        public string userName { get; set; }
    }
}
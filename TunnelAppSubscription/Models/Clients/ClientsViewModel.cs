﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Repository;

namespace TunnelApp.Subscription.Models.Clients
{
    public class ClientsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();

        public ClientsFormModel FORM { get; set; }
        public IEnumerable<Client> clients { get; set; }
        public IEnumerable<SelectListItem> clientTypes { get; set; }
        public Client client { get; set; }

        public ClientsViewModel() 
        {
            clients = db.Clients.OrderBy(x => x.Name);
        }

        public ClientsViewModel(string userName)
        {
            clientTypes = db.ClientTypes.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.IdClientType.ToString()
            });
        }

        public ClientsViewModel(Guid idClient)
        {
            client = db.Clients.Single(x => x.id == idClient);
            FORM = new ClientsFormModel();
            FORM.IdClient = client.id;
            FORM.Name = client.Name;
            FORM.Website = client.Website.Trim();
            FORM.Description = client.Description;
            FORM.Color = client.HexBgColor.Trim();
            FORM.IdClientType = client.ClientType.IdClientType;
            FORM.Logo = client.LogoUrl;

            clientTypes = db.ClientTypes.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.IdClientType.ToString()
            });
        }

        public ClientsViewModel(ClientsFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            ClientType ct = db.ClientTypes.Single(x => x.IdClientType == FORM.IdClientType);

            Client nuevo = new Client
            {
                id = Guid.NewGuid(),
                Name = FORM.Name,
                Website = FORM.Website.Trim(),
                LogoUrl = FORM.Logo.Trim(),
                Description = FORM.Description,
                ClientType = ct,
                HexBgColor = FORM.Color.Trim(),
                CreatedAt = DateTime.Now,
                CreatedBy = FORM.userName,
                deleted = false
            };
            db.Clients.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            ClientType ct = db.ClientTypes.Single(x => x.IdClientType == FORM.IdClientType);

            Client c = db.Clients.Single(x => x.id == FORM.IdClient);
            c.Name = FORM.Name;
            c.Website = FORM.Website.Trim();
            c.LogoUrl = FORM.Logo.Trim();
            c.Description = FORM.Description;
            c.HexBgColor = FORM.Color.Trim();
            c.ClientType = ct;

            db.SubmitChanges();
        }

        public void delete()
        {
            Client c = db.Clients.Single(x => x.id == FORM.IdClient);
            db.Clients.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
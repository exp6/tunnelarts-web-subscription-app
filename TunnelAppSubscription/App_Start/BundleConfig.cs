﻿using System.Web;
using System.Web.Optimization;

namespace TunnelApp.Subscription.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui/jquery-ui.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jquery/block").Include(
                        "~/Scripts/jquery-ui/jquery-blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap/bootstrap*"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/custom/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/external").Include(
                        "~/Scripts/external/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/external/reporting").Include(
                      "~/Scripts/external/reporting/*.js"
                      ));

            /** ------------------ MANAGMENT -------------------- **/

            // -------- CLIENTS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/clients/list").Include(
                        "~/Scripts/custom/manager/clients/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/clients/create").Include(
                        "~/Scripts/custom/manager/clients/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/clients/edit").Include(
                        "~/Scripts/custom/manager/clients/edit.js"));

            // -------- SUBSCRIPTIONS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/list").Include(
                        "~/Scripts/custom/manager/subscriptions/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/create").Include(
                        "~/Scripts/custom/manager/subscriptions/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/edit").Include(
                        "~/Scripts/custom/manager/subscriptions/edit.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/manage-submodules").Include(
                        "~/Scripts/custom/manager/subscriptions/manage-submodules.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/consumption-list").Include(
                        "~/Scripts/custom/manager/subscriptions/listconsumption.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/subscriptions/edit-consumption").Include(
                        "~/Scripts/custom/manager/subscriptions/editconsumption.js"));

            // -------- MODULES ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/app-modules/list").Include(
                        "~/Scripts/custom/manager/modules/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/app-modules/create").Include(
                        "~/Scripts/custom/manager/modules/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/app-modules/edit").Include(
                        "~/Scripts/custom/manager/modules/edit.js"));

            // -------- USERS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/users/list").Include(
                        "~/Scripts/custom/manager/users/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/users/create").Include(
                        "~/Scripts/custom/manager/users/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/users/edit").Include(
                        "~/Scripts/custom/manager/users/edit.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/users/userEdit").Include(
                        "~/Scripts/custom/manager/users/userEdit.js"));

            // -------- PROJECTS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/projects/list").Include(
                        "~/Scripts/custom/manager/projects/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/projects/create").Include(
                        "~/Scripts/custom/manager/projects/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/projects/edit").Include(
                        "~/Scripts/custom/manager/projects/edit.js"));

            // -------- TUNNELS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/tunnels/list").Include(
                        "~/Scripts/custom/manager/tunnels/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/tunnels/create").Include(
                        "~/Scripts/custom/manager/tunnels/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/tunnels/edit").Include(
                        "~/Scripts/custom/manager/tunnels/edit.js"));

            // -------- FACES ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/faces/list").Include(
                        "~/Scripts/custom/manager/faces/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/faces/create").Include(
                        "~/Scripts/custom/manager/faces/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/faces/edit").Include(
                        "~/Scripts/custom/manager/faces/edit.js"));

            // -------- MODEL TRAINING ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/modeltraining/list").Include(
                        "~/Scripts/custom/manager/modeltraining/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/manager/modeltraining/training").Include(
                        "~/Scripts/custom/manager/modeltraining/training.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                        "~/Content/bootstrap/bootstrap*"));

            bundles.Add(new StyleBundle("~/Content/external/css").Include(
                "~/Content/external/*.css"
                ));

            bundles.Add(new StyleBundle("~/Content/external/login/css").Include(
                "~/Content/external/login/*.css"
                ));

            bundles.Add(new StyleBundle("~/Content/external/reporting/css").Include(
                "~/Content/external/reporting/*.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/external/reporting/maps/css").Include(
                        "~/Content/external/reporting/maps/*.css"
                ));
        }
    }
}
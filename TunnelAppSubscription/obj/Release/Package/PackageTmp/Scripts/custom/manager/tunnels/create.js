﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Tunnels - Create] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    initDatePicker();

    console.log("[Manager - Tunnels - Create] Variables ready...");
}

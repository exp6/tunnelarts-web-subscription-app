﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Faces - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Faces - Edit] Variables ready...");
}
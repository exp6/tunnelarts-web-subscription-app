﻿/**  
**  INIT
**/

function init() {
    console.log("[Manager - Model Training - List] Init variables...");

    initTable();

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Model Training - List] Variables ready...");
}

function initTable() {
    $('#list-manager').DataTable({
        language: {
            lengthMenu: "Show _MENU_ entries",
            zeroRecords: "No elements created.",
            info: "Showing _PAGE_ page of _PAGES_",
            infoEmpty: "Add new tunnels accessing to Tunnels - Create New",
            paginate: {
                first: "First",
                last: "Last",
                next: "Next",
                previous: "Previous"
            },
            search: "Search:",
            infoFiltered: "(Filtered of _MAX_ entries)"
        }
    });
}
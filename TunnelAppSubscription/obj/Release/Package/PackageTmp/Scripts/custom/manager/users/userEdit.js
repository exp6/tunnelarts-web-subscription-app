﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Users - Edit User Data] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Users - Edit User Data] Variables ready...");
}
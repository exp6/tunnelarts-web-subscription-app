﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Subscriptions - Manage Submodules] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Subscriptions - Manage Submodules] Variables ready...");
}

$("#asociar-cliente").click(function () {

    if ($("#clientes-disponibles li").length == 0) return false;
    if ($("#clientes-disponibles li.active").length == 0) return false;

    var clienteId = $("#clientes-disponibles li.active").attr("id");
    var clienteNombre = $("#clientes-disponibles li.active").text();

    var disponibles = $("#clientes-disponibles");
    var asociados = $("#clientes-asociados");

    var asociadoVacio = $("#vacio-asociados p");

    if (asociadoVacio.length > 0) {
        asociadoVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    disponibles.find("#" + clienteId + "").remove();
    asociados.append(elemento);

    if ($("#clientes-disponibles li").length == 0) $("#vacio-disponibles").append($("<p/>").attr("class", "list-group-item").text("No Sub Modules are available now on this Module"));
});

$("#desasociar-cliente").click(function () {
    if ($("#clientes-asociados li").length == 0) return false;
    if ($("#clientes-asociados li.active").length == 0) return false;

    var clienteId = $("#clientes-asociados li.active").attr("id");
    var clienteNombre = $("#clientes-asociados li.active").text();

    var disponibles = $("#clientes-disponibles");
    var asociados = $("#clientes-asociados");

    var disponibleVacio = $("#vacio-disponibles p");

    if (disponibleVacio.length > 0) {
        disponibleVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    asociados.find("#" + clienteId + "").remove();
    disponibles.append(elemento);

    if ($("#clientes-asociados li").length == 0) $("#vacio-asociados").append($("<p/>").attr("class", "list-group-item").text("No Sub Modules were added to this Subscription"));
});

$("#guardar-clientes").click(function () {
    var CLIENTES = [];

    if ($("#clientes-asociados li").length > 0) {
        $("#clientes-asociados li").each(function (index, element) {
            var clientesStr = $(element).attr("id");
            CLIENTES.push(clientesStr);
        });
        var clientesJoin = CLIENTES.join(";");
        $("#FORM_SubModulesIds").val(clientesJoin);
    }

    console.log(CLIENTES);

    $("form").submit();
});
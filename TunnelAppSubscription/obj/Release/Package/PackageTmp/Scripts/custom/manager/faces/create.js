﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Faces - Create] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    initDatePicker();

    console.log("[Manager - Faces - Create] Variables ready...");
}

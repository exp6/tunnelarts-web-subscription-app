﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Subscriptions - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Subscriptions - Edit] Variables ready...");
}

function initDatePicker() {
    $('.datepickershow').datepicker('remove');

    $('.datepickershow').datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date(),
        language: 'es'
    });
};

$('.dateButton111').click(function () {
    $(this).parent().find('.datepickershow').datepicker("show");
});
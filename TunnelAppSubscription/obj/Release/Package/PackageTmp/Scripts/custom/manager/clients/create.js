﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Clients - Create] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Clients - Create] Variables ready...");
}
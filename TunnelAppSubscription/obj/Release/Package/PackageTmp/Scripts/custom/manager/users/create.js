﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Users - Create] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Users - Create] Variables ready...");
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.AppModules;

namespace TunnelApp.Subscription.Controllers
{
    public class AppModulesController : Controller
    {
        //
        // GET: /AppModules/

        public ActionResult Index()
        {
            AppModulesViewModel model = new AppModulesViewModel();
            return View(model);
        }

        //
        // GET: /AppModules/Create
        [HttpGet]
        public ActionResult Create()
        {
            AppModulesViewModel model = new AppModulesViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /AppModules/Create
        [HttpPost]
        public ActionResult Create(AppModulesFormModel form)
        {
            AppModulesViewModel model = new AppModulesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Module created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new AppModulesViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /AppModules/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdClientSubscription)
        {
            AppModulesViewModel model = new AppModulesViewModel(IdClientSubscription);
            return View(model);
        }

        //
        // POST: /AppModules/Editar
        [HttpPost]
        public ActionResult Edit(AppModulesFormModel form)
        {
            AppModulesViewModel model = new AppModulesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                //model = new AppModulesViewModel(form.IdClientSubscription);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /AppModules/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdClientSubscription)
        {
            AppModulesViewModel model = new AppModulesViewModel(IdClientSubscription);
            model.delete();
            Mensaje = "Module deleted successfully.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Users;

namespace TunnelApp.Subscription.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/

        public ActionResult Index()
        {
            UsersViewModel model = new UsersViewModel();
            return View(model);
        }

        //
        // GET: /Users/Create
        [HttpGet]
        public ActionResult Create()
        {
            UsersViewModel model = new UsersViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public ActionResult Create(UsersFormModel form)
        {
            UsersViewModel model = new UsersViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.createMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        return View(model);
                    }

                    model.create();
                    Mensaje = "Client User created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Users/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdUser)
        {
            UsersViewModel model = new UsersViewModel(IdUser);
            return View(model);
        }

        //
        // POST: /Users/Edit
        [HttpPost]
        public ActionResult Edit(UsersFormModel form)
        {
            UsersViewModel model = new UsersViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";

                    if (User.Identity.Name.ToLower() != form.DbUserName.ToLower())
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception e)
            {
                model = new UsersViewModel(form.IdUser);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }


        //
        // GET: /Users/EditUser
        [HttpGet]
        public ActionResult EditUser(Guid IdUser)
        {
            UsersViewModel model = new UsersViewModel(IdUser);
            return View(model);
        }

        //
        // POST: /Users/EditUser
        [HttpPost]
        public ActionResult EditUser(UsersFormModel form)
        {
            UsersViewModel model = new UsersViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.updateMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        return View(model);
                    }

                    model.updateUser();

                    if (User.Identity.Name.ToLower() == form.DbUserName.ToLower())
                    {
                        Mensaje = "Changes saved successfully. You have to sign in again for changes take effect.";
                        return RedirectToAction("LogOff", "Account");
                    }

                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new UsersViewModel(form.IdUser);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Users/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdUser)
        {
            UsersViewModel model = new UsersViewModel(IdUser);
            model.delete();
            Mensaje = "Subscription deleted successfully.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Users/ResetPassword
        [HttpGet]
        public ActionResult ResetPassword(Guid IdUser)
        {
            UsersViewModel model = new UsersViewModel(IdUser);
            model.resetPassword();
            Mensaje = "Password reset successfully. For more information, check the icon on Reset Password column.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

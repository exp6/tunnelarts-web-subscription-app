﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Clients;

namespace TunnelApp.Subscription.Controllers
{
    public class ClientsController : Controller
    {
        //
        // GET: /Clients/
        public ActionResult Index()
        {
            ClientsViewModel model = new ClientsViewModel();
            return View(model);
        }

        //
        // GET: /Clients/Create
        [HttpGet]
        public ActionResult Create()
        {
            ClientsViewModel model = new ClientsViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Clients/Create
        [HttpPost]
        public ActionResult Create(ClientsFormModel form)
        {
            ClientsViewModel model = new ClientsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Client created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ClientsViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Clients/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdClient)
        {
            ClientsViewModel model = new ClientsViewModel(IdClient);
            return View(model);
        }

        //
        // POST: /Clients/Editar
        [HttpPost]
        public ActionResult Edit(ClientsFormModel form)
        {
            ClientsViewModel model = new ClientsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ClientsViewModel(form.IdClient);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Clients/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdClient)
        {
            ClientsViewModel model = new ClientsViewModel(IdClient);
            model.delete();
            Mensaje = "Client deleted successfully.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

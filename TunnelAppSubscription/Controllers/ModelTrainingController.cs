﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.ModelTraining;

namespace TunnelAppSubscription.Controllers
{
    public class ModelTrainingController : Controller
    {
        //
        // GET: /ModelTraining/

        public ActionResult Index()
        {
            ModelTrainingViewModel model = new ModelTrainingViewModel();
            return View(model);
        }

        //
        // GET: /ModelTraining/Training
        [HttpGet]
        public ActionResult Training(Guid IdTunnel)
        {
            ModelTrainingViewModel model = new ModelTrainingViewModel(IdTunnel);
            return View(model);
        }

        //
        // POST: /ModelTraining/Training
        [HttpPost]
        public ActionResult Save(double rmse, Guid tunnelId)
        {
            ModelTrainingViewModel model = new ModelTrainingViewModel(tunnelId, rmse);
            int res = model.saveData();

            switch (res)
            {
                case 1:
                    Mensaje = "Prediction training updated successfully.";
                    break;
                default:
                    Mensaje = "Prediction training created successfully.";
                    break;
            }

            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetCsvTemplate()
        {
            string templatePath = @"~\Content\external\prediction\tunnel_history_template.csv";
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(templatePath));
            string fileName = "tunnel_history_template.csv";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

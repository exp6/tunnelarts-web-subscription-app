﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Tunnels;

namespace TunnelApp.Controllers
{
    public class TunnelsController : Controller
    {
        //
        // GET: /Tunnels/
        public ActionResult Index()
        {
            TunnelsViewModel model = new TunnelsViewModel();
            return View(model);
        }

        //
        // GET: /Tunnels/Create
        [HttpGet]
        public ActionResult Create()
        {
            TunnelsViewModel model = new TunnelsViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Tunnels/Create
        [HttpPost]
        public ActionResult Create(TunnelsFormModel form)
        {
            TunnelsViewModel model = new TunnelsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Tunnel created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new TunnelsViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Tunnels/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdTunnel)
        {
            TunnelsViewModel model = new TunnelsViewModel(IdTunnel);
            return View(model);
        }

        //
        // POST: /Tunnels/Editar
        [HttpPost]
        public ActionResult Edit(TunnelsFormModel form)
        {
            TunnelsViewModel model = new TunnelsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new TunnelsViewModel(form.IdTunnel);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Tunnels/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdTunnel)
        {
            TunnelsViewModel model = new TunnelsViewModel(IdTunnel);
            model.delete();
            Mensaje = "Tunnel deleted successfully.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

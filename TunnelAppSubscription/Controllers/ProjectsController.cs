﻿using RDotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Projects;

namespace TunnelApp.Subscription.Controllers
{
    public class ProjectsController : Controller
    {
        //
        // GET: /Projects/
        public ActionResult Index()
        {
            ProjectsViewModel model = new ProjectsViewModel();
            return View(model);
        }

        //
        // GET: /Projects/Create
        [HttpGet]
        public ActionResult Create()
        {
            
            ProjectsViewModel model = new ProjectsViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Projects/Create
        [HttpPost]
        public ActionResult Create(ProjectsFormModel form)
        {
            
            ProjectsViewModel model = new ProjectsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Project created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ProjectsViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Projects/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdProject)
        {
            
            ProjectsViewModel model = new ProjectsViewModel(IdProject);
            return View(model);
        }

        //
        // POST: /Projects/Editar
        [HttpPost]
        public ActionResult Edit(ProjectsFormModel form)
        {
            
            ProjectsViewModel model = new ProjectsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ProjectsViewModel(form.IdProject);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Projects/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdProject)
        {
            ProjectsViewModel model = new ProjectsViewModel(IdProject);
            model.delete();
            Mensaje = "Project deleted successfully.";

            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

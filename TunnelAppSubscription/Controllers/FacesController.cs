﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Faces;

namespace TunnelAppSubscription.Controllers
{
    public class FacesController : Controller
    {
        //
        // GET: /Faces/

        public ActionResult Index()
        {
            FacesViewModel model = new FacesViewModel();
            return View(model);
        }

        //
        // GET: /Faces/Create
        [HttpGet]
        public ActionResult Create()
        {
            FacesViewModel model = new FacesViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Faces/Create
        [HttpPost]
        public ActionResult Create(FacesFormModel form)
        {
            FacesViewModel model = new FacesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Face created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new FacesViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Faces/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdFace)
        {
            FacesViewModel model = new FacesViewModel(IdFace);
            return View(model);
        }

        //
        // POST: /Faces/Editar
        [HttpPost]
        public ActionResult Edit(FacesFormModel form)
        {
            FacesViewModel model = new FacesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new FacesViewModel(form.IdFace);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Faces/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdFace)
        {
            FacesViewModel model = new FacesViewModel(IdFace);
            model.delete();
            Mensaje = "Face deleted successfully.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

    }
}

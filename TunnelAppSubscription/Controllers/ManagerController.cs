﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TunnelApp.Subscription.Controllers
{
    public class ManagerController : Controller
    {
        //
        // GET: /Manager/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Manager/Clients

        public ActionResult Clients()
        {
            return RedirectToAction("Index", "Clients");
        }

        //
        // GET: /Manager/Subscriptions

        public ActionResult Subscriptions()
        {
            return RedirectToAction("Index", "Subscriptions");
        }

        //
        // GET: /Manager/AppModules

        public ActionResult AppModules()
        {
            return RedirectToAction("Index", "AppModules");
        }

        //
        // GET: /Manager/Projects

        public ActionResult Projects()
        {
            return RedirectToAction("Index", "Projects");
        }

        //
        // GET: /Manager/Tunnels

        public ActionResult Tunnels()
        {
            return RedirectToAction("Index", "Tunnels");
        }

        //
        // GET: /Manager/Faces

        public ActionResult Faces()
        {
            return RedirectToAction("Index", "Faces");
        }

        //
        // GET: /Manager/Users

        public ActionResult Users()
        {
            return RedirectToAction("Index", "Users");
        }

        //
        // GET: /Manager/ModelTraining

        public ActionResult ModelTraining()
        {
            return RedirectToAction("Index", "ModelTraining");
        }
    }
}

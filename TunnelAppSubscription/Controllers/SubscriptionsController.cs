﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Subscription.Models.Subscriptions;
using TunnelApp.Subscription.Helpers;
using TunnelAppSubscription.Models.Subscriptions;

namespace TunnelApp.Subscription.Controllers
{
    public class SubscriptionsController : Controller
    {
        //
        // GET: /Subscriptions/

        public ActionResult Index()
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel();
            return View(model);
        }

        //
        // GET: /Subscriptions/Create
        [HttpGet]
        public ActionResult Create()
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Subscriptions/Create
        [HttpPost]
        public ActionResult Create(SubscriptionsFormModel form)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Subscription created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new SubscriptionsViewModel(User.Identity.Name);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Subscriptions/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdClientSubscription)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(IdClientSubscription);
            return View(model);
        }

        //
        // POST: /Subscriptions/Editar
        [HttpPost]
        public ActionResult Edit(SubscriptionsFormModel form)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new SubscriptionsViewModel(form.IdClientSubscription);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Subscriptions/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdClientSubscription)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(IdClientSubscription);
            model.delete();
            Mensaje = "Subscription deleted successfully.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Subscriptions/AddSubModules
        [HttpGet]
        public ActionResult AddSubModules(Guid IdClientSubscription)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(IdClientSubscription);
            return View(model);
        }

        //
        // POST: /Subscriptions/AddSubModules
        [HttpPost]
        public ActionResult AddSubModules(SubscriptionsFormModel form)
        {
            SubscriptionsViewModel model = new SubscriptionsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.addSubModules();
                    Mensaje = "Sub-Modules saved successfully";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new SubscriptionsViewModel(form.IdClientSubscription);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

        //
        // GET: /Subscriptions/GetSubscriptionSubModules?IdClientSubscription
        [HttpGet]
        public ActionResult GetSubscriptionSubModules(Guid IdClientSubscription)
        {
            try
            {
                var categoryClients = Utils.getDBConnection().ClientSubscriptionModules.OrderBy(x => x.SubscriptionModule.Module.Name).Where(x => x.IdClientSubscription == IdClientSubscription).Select(x => new
                {
                    CategoryClient = x.SubscriptionModule.Module.Name
                }).ToList();

                return Json(categoryClients, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Subscriptions/ManageConsumption
        [HttpGet]
        public ActionResult ManageConsumption(Guid IdClientSubscription)
        {
            ConsumptionViewModel model = new ConsumptionViewModel(IdClientSubscription);
            return View(model);
        }

        //
        // GET: /Subscriptions/EditConsumption
        [HttpGet]
        public ActionResult EditConsumption(Guid IdClientSubscription, Guid IdProject)
        {
            ConsumptionViewModel model = new ConsumptionViewModel(IdClientSubscription, IdProject);
            return View(model);
        }

        //
        // POST: /Subscriptions/EditConsumption
        [HttpPost]
        public ActionResult EditConsumption(ConsumptionFormModel form)
        {
            ConsumptionViewModel model = new ConsumptionViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    model.updateChanges();
                    Mensaje = "Changes updated successfully";
                    return RedirectToAction("ManageConsumption", new { IdClientSubscription = form.IdClientSubscription });
                }
            }
            catch (Exception e)
            {
                model = new ConsumptionViewModel(form.IdClientSubscription, form.IdProject);
                Mensaje = "Couldn't save changes. Try again, internal error happened.";
            }
            return View(model);
        }
    }
}

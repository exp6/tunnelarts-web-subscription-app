﻿$(document).ready(function () {
    function startup() {
        var el = document.getElementsByTagName("canvas")[0];
        el.addEventListener("touchstart", handleStart, false);

    };

    //Define Lenguajes soportados
    var langs = ['es', 'en', 'fr', 'pt'];
    //Define el lenguaje de las fechas
    $('.ImgFlag').click(function () {
        moment.locale($(this).attr('lang'));
        iniciarDiasPag();
        iniciarMesPag();
        iniciarDatePicker();
    });

    // Inicializa Tooltips
    $('.shortcut-button').tooltip();
    $('button').tooltip();
    $('img').tooltip();
    $('li').tooltip();
    $('.dateButton').click(function () {
        $(this).parent().parent().find('.datepickershow').datepicker("show");
    });

    // Controla links
    $(".sub_menu--link").click(function () {
        window.open($(this).attr("dir"), "_self")
    });

    $("button").click(function () {
        window.open($(this).attr("dir"), "_self")
    });

    //Inicializa Tabs

    $('#Tab a:first').tab('show');

    $('#Tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    var ContQ = 0;
    //Agregar/Eliminar Input

    $('.InputAddContent .btn-success').click(function () {
        $('.InputAddContent').append('<div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-6"><div class="input-group contacto-destinatario"><select class="form-control input-sm"><option value="1">Para:</option><option value="2">CC:</option><option value="3">BCC:</option></select><span class="input-group-addon">@</span><input type="text" class="form-control" ><span class="input-group-btn"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></button></span></div></div></div>');
    });

    $('body').on('click', '.addAlt', function () {
        $(this).closest('form').find('.PanelCuestionarioAlt').append('<div class="input-group"><input type="text" class="form-control"><span class="input-group-btn"><button type="button" class="btn btn-default subAlt"><span class="glyphicon glyphicon-minus-sign"></span></button></span></div>');
    });

    $('body').on('click', '.addQ', function () {
        ContQ = ContQ + 1;
        $('.PanelCuestionarioPreguntas').append('<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading' + ContQ + '"><h4 class="panel-title"><div class="row"><div class="col-md-6"><span  class="tituloCuestionario" role="button" data-toggle="collapse" data-parent="#accordion" href="#pregunta' + ContQ + '" aria-expanded="true" aria-controls="pregunta' + ContQ + '">Pregunta ' + ContQ + '</span></div><div class="col-md-6"><button type="button" class="btn btn-danger pull-right subQ"><span class="glyphicon glyphicon-minus-sign"></span></button></div></div></h4></div><div id="pregunta' + ContQ + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + ContQ + '"><div class="panel-body"><div class="row-fluid"><form class="form-horizontal"><div class="form-group"><label class="col-sm-2 control-label">Tipo Pregunta</label><div class="col-sm-6"><select class="form-control input-sm"><option>opción</option><option>opción</option><option>opción</option></select></div></div><div class="form-group"><label class="col-sm-2 control-label">Enunciado Pregunta</label><div class="col-sm-6"><textarea class="form-control" rows="3"></textarea></div></div><div class="form-group"><i class="col-sm-4">Agregue alternativas a la pregunta</i><div class="col-sm-4"><button type="button" class="addAlt btn btn-default pull-right"><span class="glyphicon glyphicon-plus-sign"></span></button></div></div><div class="form-group"><div class="PanelCuestionarioAlt col-md-6 col-md-offset-2"></div></div></form></div></div></div></div>');
    });

    // se usa un metodo distinto para que jquery pueda leer los nuevos elementos en el DOM
    $('body').on('click', '.InputAddContent .btn-danger', function () {
        $(this).closest('.form-group').remove();
    });

    $('body').on('click', '.subAlt', function () {
        $(this).closest('.input-group').remove();
    });

    $('body').on('click', '.subQ', function () {
        $(this).closest('.panel').remove();
    });

    //Contrala evento de los select
    $('.InputAddContent').hide();
    $('#SltSucursal').change(function () {
        $('.InputAddContent').show();
    });
    $('#SltPlanificacion').change(function () {
        $('.InputAddContent').show();
    });

    $('.slcSuc').change(function () {
        $('.InputAddContent').show();
    });

    //Lanza Preview
    $('#Preview').click(function () {
        $('#ModalPreview').modal();
    });

    $('table .btn').click(function () {
        $('#ModalPreview').modal();
    });

    $('.select').children().children().click(function (e) {
        $(this).parent().parent().find('.selected-JJ').removeClass('selected-JJ');
        if ($(this).hasClass('selected-JJ')) {
            $(this).removeClass('selected-JJ');
        }
        else {
            $(this).addClass('selected-JJ');
        }
    });

    $('.deSelect').children().children().click(function (e) {
        if ($(this).hasClass('selected-JJ')) {
            $(this).removeClass('selected-JJ');
        }
        else {
            $(this).addClass('selected-JJ');
        }
    });

    $('.PanelCuestionario button').click(function (e) {
        $(this).parent().parent().find('.selected-JJ').removeClass('selected-JJ');
        $(this).parent().parent().find('.glyphicon-ok-sign').addClass('glyphicon-minus-sign');
        $(this).parent().parent().find('.glyphicon-ok-sign').removeClass('glyphicon-ok-sign');
        if ($(this).hasClass('selected-JJ')) {
            $(this).removeClass('selected-JJ');
            $(this).children().removeClass('glyphicon-ok-sign');
            $(this).children().addClass('glyphicon-minus-sign');
        }
        else {
            $(this).addClass('selected-JJ');
            $(this).children().removeClass('glyphicon-minus-sign');
            $(this).children().addClass('glyphicon-ok-sign');
        }
    });

    $('.SelectAll').click(function () {
        $('.list-plan').find('input').prop('checked', true);
    });

    $('.UnSelect').click(function () {
        $('.list-plan').find('input').prop('checked', false);
    });
});
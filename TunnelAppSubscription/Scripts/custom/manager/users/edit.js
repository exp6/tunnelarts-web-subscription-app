﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Users - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Users - Edit] Variables ready...");
}
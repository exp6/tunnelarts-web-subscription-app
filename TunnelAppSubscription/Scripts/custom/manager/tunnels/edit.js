﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Projects - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Projects - Edit] Variables ready...");
}
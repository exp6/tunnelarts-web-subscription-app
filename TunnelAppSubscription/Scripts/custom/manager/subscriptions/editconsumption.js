﻿/**  
**  INIT
**/
var totalBalance;
var totalCumulated;

function init(balance) {
    console.log("[Manager - Subscriptions - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    totalBalance = balance;

    validateInputs();

    updateDistribution();
    $("#total-balance").text((totalBalance - totalCumulated));

    console.log("[Manager - Subscriptions - Edit] Variables ready...");
}

//$('#save-changes').click(function () {



//    $("form").submit();
//});

$("#balance-project").on('input', function (e) {
    totalBalance = parseFloat(this.value);

    updateDistribution();
    $("#total-balance").text(totalBalance - totalCumulated);

    validateInputs();
});

$(".balances").on('input', function (e) {
    updateDistribution();

    if (totalBalance - totalCumulated < 0) {
        alert("Not enough balance to distribute!");
        $(this).val(0);
        updateDistribution();
    }

    $("#total-balance").text((totalBalance - totalCumulated));
});

function updateDistribution() {
    totalCumulated = 0;

    $(".balances").each(function () {
        totalCumulated += parseFloat($(this).val());
    });
}

function validateInputs() {
    if (totalBalance == 0) {
        $(".balances").prop('disabled', true);
    }
    else {
        $(".balances").prop('disabled', false);
    }
}
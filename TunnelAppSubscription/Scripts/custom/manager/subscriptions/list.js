﻿/**  
**  INIT
**/
var urlCategoriaClientes;

function init(url1) {
    console.log("[Manager - Subscriptions - List] Init variables...");

    urlCategoriaClientes = url1;
    initTable();

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Subscriptions - List] Variables ready...");
}

function initTable() {
    $('#list-manager').DataTable({
        language: {
            lengthMenu: "Show _MENU_ entries",
            zeroRecords: "No elements created.",
            info: "Showing _PAGE_ page of _PAGES_",
            infoEmpty: "Add new items accessing to Create New",
            paginate: {
                first: "First",
                last: "Last",
                next: "Next",
                previous: "Previous"
            },
            search: "Search:",
            infoFiltered: "(Filtered of _MAX_ entries)"
        }
    });
}

$('#list-manager .btn-modal-trigger-clientes').click(function () {

    var idCategoria = $(this).attr('name');
    var nombreCategoria = $(this).attr('value');

    $.getJSON(urlCategoriaClientes, {
        IdClientSubscription: idCategoria
    })
    .done(function (data) {
        $("#titulo-modal").text("Modules added to Subscription: " + nombreCategoria);
        var clientes = $("#listado-elementos-categoria");
        clientes.empty();

        var elemento;

        $.each(data, function (i, item) {
            elemento = $("<li/>").text(item.CategoryClient);
            clientes.append(elemento);
        });

        if (data.length == 0) {
            elemento = $("<li/>").text("No Modules were added to this Subscription");
            clientes.append(elemento);
        }

        $('#ModalPreviewCategoria').modal();
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    });
});
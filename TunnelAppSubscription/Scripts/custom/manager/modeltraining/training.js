﻿/**  
**  INIT
**/
var files;
var tunnelId;
var url, urlSubs;
var rmse;

function init(id, url1) {
    console.log("[Manager - Model Training - Training] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    $('input[type=file]').on('change', function () {
        files = this.files;
        
    });
    
    //url = "http://localhost/training/"; //Dev env
    url = "http://tunnelarts1669.cloudapp.net/training/"; //Prod env

    urlSubs = url1;

    tunnelId = id;

    console.log("[Manager - Model Training - Training] Variables ready...");
}

$('#train-model').on('click', function (event) {
    //event.stopPropagation(); // Stop stuff happening
    //event.preventDefault(); // Totally stop stuff happening

    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function (key, value) {
        data.append(key, value);
    });

    $.ajax({
        url: url + tunnelId + '/upload-file',
        type: 'POST',
        data: data,
        cache: false,
        dataType: "text",
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                },
                message: '<h4>Uploading CSV file...</h4>'
            });
        },
        success: function (data, textStatus, jqXHR) {
            
            if (typeof data.error === 'undefined') {
                $.unblockUI();
                trainModel();
            }
            else {
                alert('ERROR: ' + data.error);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('ERROR: ' + errorThrown);
            $.unblockUI();
        }
    });
});

function trainModel() {
    $.ajax({
        url: url + tunnelId + '/train-model',
        method: "POST",
        traditional: true,
        beforeSend: function () {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                },
                message: '<h4>Step 1 of 3: Training the model...</h4><h4>This will take a few minutes. Please wait...</h4><br>Don\'t close this page'
            });
        },
        success: function (data) {
            $.unblockUI();
            if (data.message == "OK") {
                rmse = data.rmse;
                buildPredictors();
            }
            else {
                alert("ERROR: Internal server error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            alert("ERROR: " + errorThrown);
        }
    });
}

function buildPredictors() {
    $.ajax({
        url: url + tunnelId + '/build-predictors',
        method: "POST",
        traditional: true,
        beforeSend: function () {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                },
                message: '<h4>Step 2 of 3: Creating predictors...</h4><h4>This will take a few minutes. Please wait...</h4><br>Don\'t close this page'
            });
        },
        success: function (data) {
            $.unblockUI();
            cloudUpload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            alert("ERROR: " + errorThrown);
        }
    });
}

function cloudUpload() {
    $.ajax({
        url: url + tunnelId + '/upload2cloud',
        method: "POST",
        traditional: true,
        beforeSend: function () {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                },
                message: '<h4>Step 3 of 3: Uploading predictors to cloud...</h4><h4>This will take a few minutes. Please wait...</h4><br>Don\'t close this page'
            });
        },
        success: function (data) {
            $.unblockUI();
            alert("Model Training process executed succesfully!");
            dataPersist();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            alert("ERROR: " + errorThrown);
        }
    });
}

function dataPersist() {
    $("#rmse").val(rmse);
    $("#tunnel-id").val(tunnelId);
    $("#training-data").submit();
}

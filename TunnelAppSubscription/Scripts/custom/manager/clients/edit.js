﻿/**  
**  INIT
**/
function init() {
    console.log("[Manager - Clients - Edit] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Manager - Clients - Edit] Variables ready...");
}
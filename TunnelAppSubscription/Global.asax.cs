﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TunnelApp.Subscription.Configuration;

namespace TunnelApp.Subscription
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            IList<IConfigurable> configurations = new List<IConfigurable>();
            configurations.Add(new MapperConfiguration());
            ModelBinders.Binders.Add(typeof(DateTime?), new CurrentCultureDateTimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new CurrentCultureDateTimeBinder());

            foreach (var item in configurations)
            {
                item.Configure();

            }

            TunnelApp.Subscription.WebApiConfig.Register(GlobalConfiguration.Configuration);
            TunnelApp.Subscription.FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            TunnelApp.Subscription.RouteConfig.RegisterRoutes(RouteTable.Routes);
            TunnelApp.Subscription.App_Start.BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Models.Accounts
{
    public class LoginUtils
    {
        public static User getCustomUser(string username)
        {
            TunnelAppDataContext db = new TunnelAppDataContext().WithConnectionStringFromConfiguration();

            User customUser = db.Users.Single(x => x.UserName == username);

            if (customUser == null)
            {
                customUser = new User();
                customUser.UserName = username;
            }

            return customUser;
        }

        public static string getSubscriptionClientColor(string ID)
        {
            TunnelAppDataContext db = new TunnelAppDataContext().WithConnectionStringFromConfiguration();

            ClientSubscription cs = null;

            if (db.ClientSubscriptions.Where(x => x.IdClientSubscription == Guid.Parse(ID)).Any())
            {
                cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(ID));
            }
            return (cs != null ? cs.Client.HexBgColor : "#000000");
        }
    }
}
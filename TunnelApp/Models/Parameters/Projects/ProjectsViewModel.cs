﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;
using System.Globalization;

namespace TunnelApp.Models.Parameters.Projects
{
    public class ProjectsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public ProjectsFormModel FORM { get; set; }
        public IEnumerable<Project> projects { get; set; }
        public Project project { get; set; }
        public ClientSubscription cs { get; set; }

        public ProjectsViewModel(string AppId) 
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
            projects = db.Projects.OrderBy(x => x.Name);
        }

        public ProjectsViewModel(string userName, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
        }

        public ProjectsViewModel(Guid idProject, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
            project = db.Projects.Single(x => x.IdProject == idProject);
            FORM = new ProjectsFormModel();
            FORM.IdProject = project.IdProject;
            FORM.Code = project.Code;
            FORM.Name = project.Name;
            FORM.Description = project.Description;
            FORM.startDate = project.StartDate.ToShortDateString();
            FORM.endDate = project.FinishDate.ToShortDateString();
        }

        public ProjectsViewModel(ProjectsFormModel form, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Project nuevo = new Project
            {
                IdProject = Guid.NewGuid(),
                
                Code = FORM.Code,
                Name = FORM.Name,
                Description = FORM.Description,
                StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                FinishDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                CreatedAt = DateTime.Now,
                CreatedBy = FORM.userName
            };
            db.Projects.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Project p = db.Projects.Single(x => x.IdProject == FORM.IdProject);
            p.Code = FORM.Code;
            p.Name = FORM.Name;
            p.Description = FORM.Description;
            p.StartDate = DateTime.ParseExact(FORM.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            p.FinishDate = DateTime.ParseExact(FORM.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            db.SubmitChanges();
        }

        public void delete()
        {
            Project p = db.Projects.Single(x => x.IdProject == FORM.IdProject);
            db.Projects.DeleteOnSubmit(p);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Models.Parameters.Faces
{
    public class FacesViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public FacesFormModel FORM { get; set; }
        public IEnumerable<Face> faces { get; set; }
        public Face face { get; set; }

        public FacesViewModel() 
        {
            faces = db.Faces.OrderBy(x => x.Name);
        }

        public FacesViewModel(string userName)
        {
        }

        public FacesViewModel(Guid idFace)
        {
            face = db.Faces.Single(x => x.IdFace == idFace);
            FORM = new FacesFormModel();
            //FORM.IdClient = client.IdClient;
            //FORM.Nombre = client.Nombre;
            //FORM.CorreoContacto = client.CorreoContacto;
            //FORM.Telefono = client.Telefono;
        }

        public FacesViewModel(FacesFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Face nuevo = new Face
            {
                IdFace = Guid.NewGuid(),
                //Nombre = FORM.Nombre,
                //CorreoContacto = FORM.CorreoContacto,
                //Telefono = FORM.Telefono,
                //Estado = 1
            };
            db.Faces.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            //Face f = db.Faces.Single(x => x.IdFace == FORM.idFace);
            //d.Nombre = FORM.Nombre;
            //d.CorreoContacto = FORM.CorreoContacto;
            //d.Telefono = FORM.Telefono;
            db.SubmitChanges();
        }

        public void delete()
        {
            //Face f = db.Faces.Single(x => x.IdFace == FORM.idFace);
            //db.Faces.DeleteOnSubmit(f);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
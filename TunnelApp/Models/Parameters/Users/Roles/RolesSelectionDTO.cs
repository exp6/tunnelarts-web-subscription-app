﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunnelApp.Models.Parameters.Users.Roles
{
    public class RolesSelectionDTO
    {
        public string SelectedRole { get; set; }
        public bool Checked { get; set; }
        public Guid IdSelectedRole { get; set; }
    }
}
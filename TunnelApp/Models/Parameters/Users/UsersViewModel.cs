﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TunnelApp.Repository;

using TunnelApp.Models.Parameters.Users.Roles;
using TunnelApp.Models.Accounts;

namespace TunnelApp.Models.Parameters.Users
{
    public class UsersViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public UsersFormModel FORM { get; set; }
        public IEnumerable<User> Users { get; set; }
        public User user { get; set; }
        public List<RolesSelectionDTO> userRoles { get; set; }

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        public IEnumerable<SelectListItem> clients { get; set; }

        public ClientSubscription cs { get; set; }

        public UsersViewModel(string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
            Users = db.Users.OrderBy(x => x.FirstName).Where(x => x.Enabled == true && x.IdClient == cs.Client.IdClient);
        }

        public UsersViewModel(string userName, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));

            FORM = new UsersFormModel();
            FORM.SelectedRoles = getAllRoles();
            FORM.IdClient = cs.Client.IdClient;

            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.IdClient.ToString()
            });
        }

        public UsersViewModel(Guid IdUser, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));

            user = db.Users.Single(x => x.IdUser == IdUser);
            FORM = new UsersFormModel();
            FORM.FirstName = user.FirstName;
            FORM.LastName = user.LastName;
            FORM.DbUserName = user.UserName;
            FORM.Contact = user.Contact;
            FORM.Email = user.Email;
            FORM.IdUser = user.IdUser;
            FORM.IdClient = cs.Client.IdClient;
            FORM.SelectedRoles = getAllRoles(user.IdUser);

            clients = db.Clients.OrderBy(x => x.Name).Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.IdClient.ToString()
            });
        }

        private List<RolesSelectionDTO> getAllRoles()
        {
            List<Role> roles = db.Roles.ToList();
            userRoles = new List<RolesSelectionDTO>();
            RolesSelectionDTO dto;

            foreach (Role rol in roles)
            {
                dto = new RolesSelectionDTO
                {
                    SelectedRole = rol.Name,
                    Checked = true,
                    IdSelectedRole = rol.IdRole
                };
                userRoles.Add(dto);
            }
            return userRoles;
        }

        private List<RolesSelectionDTO> getAllRoles(Guid IdUser)
        {
            IQueryable<Role> roles = db.Roles;
            List<Guid?> rolesUsuario = db.UserRoles.Where(x => x.IdUser == IdUser).Select(x => x.IdRole).ToList();
            userRoles = new List<RolesSelectionDTO>();

            RolesSelectionDTO dto;

            foreach (Role rol in roles)
            {
                dto = new RolesSelectionDTO
                {
                    SelectedRole = rol.Name,
                    Checked = rolesUsuario.Contains(rol.IdRole),
                    IdSelectedRole = rol.IdRole
                };
                userRoles.Add(dto);
            }
            return userRoles;
        }

        public UsersViewModel(UsersFormModel form, string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/
        private void initializeMembershipContext()
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }
        }

        public string createMembership()
        {
            initializeMembershipContext();
            MembershipCreateStatus createStatus = MembershipService.CreateUser(FORM.DbUserName.ToLower(), FORM.Password, FORM.Email);

            string errorMessage = string.Empty;

            if (createStatus == MembershipCreateStatus.DuplicateUserName)
                errorMessage = "Duplicated User Name. Choose another.";
            if (createStatus == MembershipCreateStatus.DuplicateEmail)
                errorMessage = "Duplicated Email. Choose another.";
            if (createStatus == MembershipCreateStatus.InvalidPassword)
                errorMessage = "Invalid Password. Length has to be at least 6 characters long.";

            return errorMessage;
        }


        public void create(string AppId)
        {
            cs = db.ClientSubscriptions.Single(x => x.IdClientSubscription == Guid.Parse(AppId));

            User newUser = new User
            {
                IdUser = Guid.NewGuid(),
                FirstName = FORM.FirstName,
                UserName = FORM.DbUserName.ToLower(),
                LastName = FORM.LastName,
                Contact = FORM.Contact ?? "",
                Email = FORM.Email,
                Enabled = true,
                Client = cs.Client,
                CreatedBy = FORM.UserName,
                CreatedAt = DateTime.Now
            };

            List<RolesSelectionDTO> rolesSeleccionados = FORM.SelectedRoles.Where(x => x.Checked).ToList();
            List<UserRole> rolesUsuario = new List<UserRole>();
            UserRole ru;
            Role rol;
            List<string> rolesStr = new List<string>();

            foreach (RolesSelectionDTO rs in rolesSeleccionados)
            {
                rol = db.Roles.Single(x => x.IdRole == rs.IdSelectedRole);

                ru = new UserRole
                {
                    IdUserRole = Guid.NewGuid(),
                    Role = rol,
                    User = newUser
                };

                rolesStr.Add(rol.Name.ToLower());
                rolesUsuario.Add(ru);
            }

            System.Web.Security.Roles.AddUserToRoles(FORM.DbUserName.ToLower(), rolesStr.ToArray());
            db.UserRoles.InsertAllOnSubmit(rolesUsuario);
            db.SubmitChanges();
        }

        public void update()
        {
            User u = db.Users.Where(x => x.IdUser == FORM.IdUser).SingleOrDefault();
            u.FirstName = FORM.FirstName;
            u.LastName = FORM.LastName;
            u.Contact = FORM.Contact ?? "";
            u.Email = FORM.Email;

            MembershipUser mUser = Membership.GetUser(FORM.DbUserName.ToLower());
            mUser.Email = FORM.Email;
            Membership.UpdateUser(mUser);

            db.SubmitChanges();
        }

        public void updateUser()
        {
            User usuario = db.Users.Single(x => x.IdUser == FORM.IdUser);
            List<Guid?> rolesUsuario = db.UserRoles.Where(x => x.IdUser == usuario.IdUser).Select(x => x.IdRole).ToList();
            UserRole usuarioRol;
            Role r;

            for (int i = 0; i < FORM.SelectedRoles.Count(); i++)
            {
                r = db.Roles.Single(x => x.IdRole == FORM.SelectedRoles[i].IdSelectedRole);

                if (FORM.SelectedRoles[i].Checked)
                {
                    if (!rolesUsuario.Contains(r.IdRole))
                    {
                        usuarioRol = new UserRole
                        {
                            IdUserRole = Guid.NewGuid(),
                            User = usuario,
                            Role = r
                        };

                        db.UserRoles.InsertOnSubmit(usuarioRol);
                    }
                }
                else
                {
                    if (rolesUsuario.Contains(r.IdRole))
                    {
                        usuarioRol = db.UserRoles.Single(x => x.Role == r && x.User == usuario);

                        db.UserRoles.DeleteOnSubmit(usuarioRol);
                    }
                }
            }

            db.SubmitChanges();
        }

        public string updateMembership()
        {
            User usuario = db.Users.Single(x => x.IdUser == FORM.IdUser);

            initializeMembershipContext();
            string errorMessage = string.Empty;

            Role r;
            string[] rolesUsuario = System.Web.Security.Roles.GetRolesForUser(usuario.UserName.ToLower());
            rolesUsuario = rolesUsuario.Select(s => s.ToLowerInvariant()).ToArray();

            for (int i = 0; i < FORM.SelectedRoles.Count(); i++)
            {
                r = db.Roles.Single(x => x.IdRole == FORM.SelectedRoles[i].IdSelectedRole);

                if (FORM.SelectedRoles[i].Checked)
                {
                    if (!rolesUsuario.Contains(r.Name.ToLower()))
                    {
                        System.Web.Security.Roles.AddUserToRole(usuario.UserName.ToLower(), r.Name.ToLower());
                    }
                }
                else
                {
                    if (rolesUsuario.Contains(FORM.SelectedRoles[i].SelectedRole.ToLower()))
                    {
                        System.Web.Security.Roles.RemoveUserFromRole(usuario.UserName.ToLower(), r.Name.ToLower());
                    }
                }
            }

            return errorMessage;
        }

        public void delete()
        {
            List<UserRole> ur = db.UserRoles.Where(x => x.IdUser == FORM.IdUser).ToList();

            db.UserRoles.DeleteAllOnSubmit(ur);

            User u = db.Users.Single(x => x.IdUser == FORM.IdUser);

            db.Users.DeleteOnSubmit(u);

            deleteMembership(u);

            db.SubmitChanges();
        }

        private void deleteMembership(User u)
        {
            Membership.DeleteUser(u.UserName.ToLower(), true);
        }

        public void resetPassword()
        {
            User u = db.Users.Single(x => x.IdUser == FORM.IdUser);
            MembershipUser user = Membership.GetUser(u.UserName.ToLower());
            user.IsApproved = true;
            user.UnlockUser();
            string buff = user.ResetPassword();
            user.ChangePassword(buff, u.UserName.ToLower() + "@skava");
            Membership.UpdateUser(user);
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}
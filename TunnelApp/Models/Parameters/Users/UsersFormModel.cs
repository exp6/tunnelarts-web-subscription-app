﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TunnelApp.Models.Parameters.Users.Roles;

namespace TunnelApp.Models.Parameters.Users
{
    public class UsersFormModel
    {
        public Guid IdUser { get; set; }

        [Display(Name = "User Name")]
        public string DbUserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Contact")]
        public string Contact { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Old Password")]
        public string OldPassword { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "User Roles")]
        public List<RolesSelectionDTO> SelectedRoles { get; set; }

        [Display(Name = "Change Password?")]
        public bool PasswordChange { get; set; }

        [Display(Name = "Client")]
        public Guid IdClient { get; set; }

        public string UserName { get; set; }
    }
}
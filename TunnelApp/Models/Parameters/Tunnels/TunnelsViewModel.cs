﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TunnelApp.Repository;

namespace TunnelApp.Models.Parameters.Tunnels
{
    public class TunnelsViewModel
    {
        private TunnelAppDataContext db = new TunnelAppDataContext()
                .WithConnectionStringFromConfiguration();
        public TunnelsFormModel FORM { get; set; }
        public IEnumerable<Tunnel> tunnels { get; set; }
        public Tunnel tunnel { get; set; }

        public TunnelsViewModel() 
        {
            //tunnels = db.Tunnels.OrderBy(x => x.Name);
        }

        public TunnelsViewModel(string userName)
        {
        }

        public TunnelsViewModel(Guid idTunnel)
        {
            tunnel = db.Tunnels.Single(x => x.IdTunnel == idTunnel);
            FORM = new TunnelsFormModel();
            //FORM.IdClient = client.IdClient;
            //FORM.Nombre = client.Nombre;
            //FORM.CorreoContacto = client.CorreoContacto;
            //FORM.Telefono = client.Telefono;
        }

        public TunnelsViewModel(TunnelsFormModel form)
        {
            FORM = form;
        }

        /**
         * CRUD Functions
        **/

        public void create()
        {
            Tunnel nuevo = new Tunnel
            {
                IdTunnel = Guid.NewGuid(),
                //Nombre = FORM.Nombre,
                //CorreoContacto = FORM.CorreoContacto,
                //Telefono = FORM.Telefono,
                //Estado = 1
            };
            db.Tunnels.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            //Tunnel t = db.Tunnels.Single(x => x.IdTunnel == FORM.idTunnel);
            //d.Nombre = FORM.Nombre;
            //d.CorreoContacto = FORM.CorreoContacto;
            //d.Telefono = FORM.Telefono;
            db.SubmitChanges();
        }

        public void delete()
        {
            //Tunnel t = db.Tunnels.Single(x => x.IdTunnel == FORM.idTunnel);
            //db.Tunnels.DeleteOnSubmit(t);
            db.SubmitChanges();
        }

        /**
         * END CRUD Functions
        **/
    }
}
﻿/**  
**  INIT
**/
function init() {
    console.log("[Parameters - Projects - Create] Init variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    initDatePicker();

    console.log("[Parameters - Projects - Create] Variables ready...");
}

function initDatePicker() {
    $('.datepickershow').datepicker('remove');

    $('.datepickershow').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        language: 'es'
    });
};

$('.dateButton111').click(function () {
    $(this).parent().find('.datepickershow').datepicker("show");
});
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Models.Parameters.Users;

namespace TunnelApp.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/

        public ActionResult Index()
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(AppId);
            return View(model);
        }

        //
        // GET: /Users/Create
        [HttpGet]
        public ActionResult Create()
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(User.Identity.Name, AppId);
            return View(model);
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public ActionResult Create(UsersFormModel form)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(form, AppId);
            
            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.createMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        model = new UsersViewModel(form, AppId);
                        return View(model);
                    }

                    model.create(AppId);
                    Mensaje = "Se ha creado al usuario con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Users/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdUser)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(IdUser, AppId);
            return View(model);
        }

        //
        // POST: /Users/Edit
        [HttpPost]
        public ActionResult Edit(UsersFormModel form)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(form, AppId);
            
            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";

                    if (User.Identity.Name.ToLower() != form.DbUserName.ToLower())
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception e)
            {
                model = new UsersViewModel(form.IdUser, AppId);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }


        //
        // GET: /Users/EditUser
        [HttpGet]
        public ActionResult EditUser(Guid IdUser)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(IdUser, AppId);
            return View(model);
        }

        //
        // POST: /Users/EditUser
        [HttpPost]
        public ActionResult EditUser(UsersFormModel form)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(form, AppId);
            
            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.updateMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        return View(model);
                    }

                    model.updateUser();

                    if (User.Identity.Name.ToLower() == form.DbUserName.ToLower())
                    {
                        Mensaje = "Se han guardado los cambios con éxito. Favor ingrese nuevamente al sistema.";
                        return RedirectToAction("LogOff", "Account");
                    }

                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new UsersViewModel(form.IdUser, AppId);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Users/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdUser)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(IdUser, AppId);
            model.delete();
            Mensaje = "Se ha eliminado al usuario con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Users/ResetPassword
        [HttpGet]
        public ActionResult ResetPassword(Guid IdUser)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            UsersViewModel model = new UsersViewModel(IdUser, AppId);
            model.resetPassword();
            Mensaje = "Se ha reseteado la contraseña con éxito. Para mayor información, haga click en el ícono que se encuentra en la columna Resetear Contraseña.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

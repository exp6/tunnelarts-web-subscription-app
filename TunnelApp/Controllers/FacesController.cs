﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Models.Parameters.Faces;

namespace TunnelApp.Controllers
{
    public class FacesController : Controller
    {
        //
        // GET: /Faces/
        public ActionResult Index()
        {
            FacesViewModel model = new FacesViewModel();
            return View(model);
        }

        //
        // GET: /Faces/Create
        [HttpGet]
        public ActionResult Create()
        {
            FacesViewModel model = new FacesViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Faces/Create
        [HttpPost]
        public ActionResult Create(FacesFormModel form)
        {
            FacesViewModel model = new FacesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado al distribuidor con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new FacesViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Faces/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdDistribuidor)
        {
            FacesViewModel model = new FacesViewModel(IdDistribuidor);
            return View(model);
        }

        //
        // POST: /Faces/Editar
        [HttpPost]
        public ActionResult Edit(FacesFormModel form)
        {
            FacesViewModel model = new FacesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                //model = new FacesViewModel(form.IdDistribuidorRepuestos);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Faces/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdDistribuidor)
        {
            FacesViewModel model = new FacesViewModel(IdDistribuidor);
            model.delete();
            Mensaje = "Se ha eliminado al distribuidor con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

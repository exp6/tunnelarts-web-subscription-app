﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TunnelApp.Controllers
{
    public class ParametersController : Controller
    {
        //
        // GET: /Parameters/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Parameters/Projects

        public ActionResult Projects()
        {
            return RedirectToAction("Index", "Projects");
        }

        //
        // GET: /Parameters/Tunnels

        public ActionResult Tunnels()
        {
            return RedirectToAction("Index", "Tunnels");
        }

        //
        // GET: /Parameters/Faces

        public ActionResult Faces()
        {
            return RedirectToAction("Index", "Faces");
        }

        //
        // GET: /Parameters/SupportRecommendations

        public ActionResult SupportRecommendations()
        {
            return RedirectToAction("Index", "SupportRecommendations");
        }

        //
        // GET: /Parameters/Users

        public ActionResult Users()
        {
            return RedirectToAction("Index", "Users");
        }

    }
}

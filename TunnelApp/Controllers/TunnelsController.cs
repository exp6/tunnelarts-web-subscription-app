﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Models.Parameters.Tunnels;

namespace TunnelApp.Controllers
{
    public class TunnelsController : Controller
    {
        //
        // GET: /Tunnels/
        public ActionResult Index()
        {
            TunnelsViewModel model = new TunnelsViewModel();
            return View(model);
        }

        //
        // GET: /Tunnels/Create
        [HttpGet]
        public ActionResult Create()
        {
            TunnelsViewModel model = new TunnelsViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Tunnels/Create
        [HttpPost]
        public ActionResult Create(TunnelsFormModel form)
        {
            TunnelsViewModel model = new TunnelsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado al distribuidor con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new TunnelsViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Tunnels/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdDistribuidor)
        {
            TunnelsViewModel model = new TunnelsViewModel(IdDistribuidor);
            return View(model);
        }

        //
        // POST: /Tunnels/Editar
        [HttpPost]
        public ActionResult Edit(TunnelsFormModel form)
        {
            TunnelsViewModel model = new TunnelsViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    //form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                //model = new TunnelsViewModel(form.IdDistribuidorRepuestos);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Tunnels/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdDistribuidor)
        {
            TunnelsViewModel model = new TunnelsViewModel(IdDistribuidor);
            model.delete();
            Mensaje = "Se ha eliminado al distribuidor con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

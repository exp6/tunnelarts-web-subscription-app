﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelApp.Models.Parameters.Projects;

namespace TunnelApp.Controllers
{
    public class ProjectsController : Controller
    {
        //
        // GET: /Projects/
        public ActionResult Index()
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(AppId);
            return View(model);
        }

        //
        // GET: /Projects/Create
        [HttpGet]
        public ActionResult Create()
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(User.Identity.Name, AppId);
            return View(model);
        }

        //
        // POST: /Projects/Create
        [HttpPost]
        public ActionResult Create(ProjectsFormModel form)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(form, AppId);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Subscription created successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ProjectsViewModel(User.Identity.Name, AppId);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Projects/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdProject)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(IdProject, AppId);
            return View(model);
        }

        //
        // POST: /Projects/Editar
        [HttpPost]
        public ActionResult Edit(ProjectsFormModel form)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(form, AppId);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Changes saved successfully.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ProjectsViewModel(form.IdProject, AppId);
                Mensaje = "Changes couldn't be saved. Please, try again later. If the problem persist, please contact to Support Help Desk.";
            }
            return View(model);
        }

        //
        // GET: /Projects/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdProject)
        {
            string AppId = HttpContext.Session["_AppID"].ToString();
            ProjectsViewModel model = new ProjectsViewModel(IdProject, AppId);
            model.delete();
            Mensaje = "Subscription deleted successfully.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}

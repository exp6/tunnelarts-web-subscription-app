﻿using System.Web;
using System.Web.Optimization;

namespace TunnelApp.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui/jquery-ui.js"
                        ));

            //bundles.Add(new ScriptBundle("~/bundles/jquery/block").Include(
            //            "~/Scripts/jquery-blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap/bootstrap*"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/custom/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/external").Include(
                        "~/Scripts/external/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/external/reporting").Include(
                       "~/Scripts/external/reporting/*.js"
                       ));

            /** ------------------ MANAGMENT -------------------- **/

            // -------- USERS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/users/list").Include(
                        "~/Scripts/custom/parameters/users/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/users/create").Include(
                        "~/Scripts/custom/parameters/users/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/users/edit").Include(
                        "~/Scripts/custom/parameters/users/edit.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/users/userEdit").Include(
                        "~/Scripts/custom/parameters/users/userEdit.js"));

            // -------- PROJECTS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/projects/list").Include(
                        "~/Scripts/custom/parameters/projects/list.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/projects/create").Include(
                        "~/Scripts/custom/parameters/projects/create.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/parameters/projects/edit").Include(
                        "~/Scripts/custom/parameters/projects/edit.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                        "~/Content/bootstrap/bootstrap*"));

            bundles.Add(new StyleBundle("~/Content/external/css").Include(
                "~/Content/external/*.css"
                ));

            bundles.Add(new StyleBundle("~/Content/external/reporting/css").Include(
                "~/Content/external/reporting/*.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/external/reporting/maps/css").Include(
                        "~/Content/external/reporting/maps/*.css"
                ));
        }
    }
}